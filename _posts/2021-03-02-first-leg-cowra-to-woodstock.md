---
layout: post
title:  "First leg: Cowra to Woodstock"
author: phr
categories: [ Lifestyle ]
image: assets/images/woodstock01.jpg
beforetoc: "Disused rail corridors are ideal spaces for converting to dedicated cycle ways!"
toc: true
---

The distance from Cowra to Woodstock is about 17km and makes a great first leg of the big trip from Cowra to Bathurst.  This "Proof of Concept" of making use of disused rail corridors is perfect! - all that needs to be done is some surfacing of the space between the two steel rail tracks to make it usable by bicyclists.  I am not sure of what the cost of this exercise would be but it would be modest and a very worthwhile project - it would be a great facility for locals in Cowra and Woodstock and it wouldattract a huge amount of media attention and this could only be good for the local tourist industry.

