---
layout: page
title: Central West (NSW) Bicycle Way
permalink: /about
comments: false
image: assets/images/screenshot.jpg
imageshadow: true
---

The aim of this site is to convert disused train corridors in the Central West of NSW into dedicated bicycle ways.  The success of the project will take a lot of enthusiastic cyclists and a lot of negotiation with relevant stakeholders eg NSW Rail, Cowra Council, Central West Tourism etc.

Contact:  phr AT centralwestbikeway DOT org DOT au

